﻿using System.Collections.Generic;
using System.Linq;
using TestNet.Entities;
using TestNet.SqlDataAccess.Entities;

namespace TestNet.Domain.Contract.Repositories
{
    public interface ICustomerRepository
    {
        Customer Add(Customer entity);

        void Delete(Customer entity);

        Customer Update(Customer entity);

        GridResponse<IList<Customer>> Find(SearchCriteria search);

        IList<Customer> GetAll();

        Customer GetById(int id);
    }
}
