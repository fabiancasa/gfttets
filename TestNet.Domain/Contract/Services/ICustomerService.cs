﻿using System.Collections.Generic;
using System.Linq;
using TestNet.Entities;
using TestNet.SqlDataAccess.Entities;

namespace TestNet.Domain.Contract.Services
{
    public interface ICustomerService
    {
        Customer Save(Customer dto);

        void Delete(Customer dto);

        IList<Customer> GetAll();

        GridResponse<IList<Customer>> Find(SearchCriteria search);

        Customer GetById(int id);
    }
}
