﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestNet.Domain.Contract.Repositories;
using TestNet.Domain.Contract.Services;
using TestNet.Entities;
using TestNet.SqlDataAccess.Entities;

namespace TestNet.Domain.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public IList<Customer> GetAll()
        {
            return _customerRepository.GetAll();
        }

        public GridResponse<IList<Customer>> Find(SearchCriteria search)
        {
            return _customerRepository.Find(search);
        }

        public Customer GetById(int id)
        {
            return _customerRepository.GetById(id);
        }

        public Customer Save(Customer customer)
        {
            Customer response;

            if (customer.Id == 0)
            {
                response = _customerRepository.Add(customer);
            }
            else
            {
                response = _customerRepository.Update(customer);
            }

            return response;
        }

        public void Delete(Customer customer)
        {
            _customerRepository.Delete(customer);
        }
    }
}
