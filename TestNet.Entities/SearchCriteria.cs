﻿namespace TestNet.Entities
{
    public class SearchCriteria
    {
        public int? PageSize { get; set; }
        public int? Skip { get; set; }
        public string Filter { get; set; }

        public SortOrder SortOrder { get; set; }

        //public string OrderBy { get; set; }
        //public bool OrderDesc
        //{
        //    get
        //    {
        //        return SortDirection.ToUpper() == "DSC" ? true : false;
        //    }
        //}

        //public string SortDirection { get; set; }
    }

    public class SortOrder
    {
        public enum SortOrderEnum
        {
            Unspecified = 0,
            Asc = 1,
            Dsc = 2
        }
        public string ColumnName { get; set; }
        public SortOrderEnum ColumnOrder { get; set; }
    }
}
