﻿using System.ComponentModel.DataAnnotations;

namespace TestNet.SqlDataAccess.Entities
{
    public class Customer
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(40)]
        [Required(ErrorMessage = "First name is required")]
        public string FirstName { get; set; }

        [MaxLength(40)]
        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }

        [MaxLength(40)]
        public string City { get; set; }

        [MaxLength(40)]
        public string Country { get; set; }

        [MaxLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

    }
}
