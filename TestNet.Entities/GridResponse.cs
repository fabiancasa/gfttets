﻿namespace TestNet.Entities
{
    public class GridResponse<T>
    {
        public T Result { get; set; }
        public int ResultCount { get; set; }
    }
}
