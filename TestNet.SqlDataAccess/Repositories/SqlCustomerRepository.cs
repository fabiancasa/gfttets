﻿using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TestNet.Domain.Contract.Repositories;
using TestNet.Entities;
using TestNet.SqlDataAccess.Entities;
using TestNet.SqlDataAccess.Extensions;

namespace TestNet.SqlDataAccess.Repositories
{
    public class SqlCustomerRepository : ICustomerRepository
    {
        private readonly TestNetContext _context;

        public SqlCustomerRepository(TestNetContext context)
        {
            _context = context;
        }

        public Customer Update(Customer customer)
        {
            Customer entity = _context.Customers.Where(x => x.Id == customer.Id).FirstOrDefault();

            if (entity != null)
            {
                entity.FirstName = customer.FirstName;
                entity.LastName = customer.LastName;
                entity.Country = customer.Country;
                entity.City = customer.City;
                entity.Phone = customer.Phone;

                _context.Entry(entity).CurrentValues.SetValues(customer);
                _context.SaveChanges();
            }

            return entity;
        }

        public Customer Add(Customer customer)
        {
            Customer entity = new Customer()
            {
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Country = customer.Country,
                City = customer.City,
                Phone = customer.Phone
            };

            _context.Customers.Add(entity);
            int id = _context.SaveChanges();

            entity.Id = id;

            return entity;
        }

        public void Delete(Customer customer)
        {
            Customer entity = _context.Customers.Where(x => x.Id == customer.Id).FirstOrDefault();

            if (entity != null)
            {
                _context.Customers.Remove(entity);
                _context.SaveChanges();
            }
        }

        public GridResponse<IList<Customer>> Find(SearchCriteria search)
        {
            var predicate = PredicateBuilder.New<Customer>(true);

            if (!string.IsNullOrEmpty(search.Filter))
            {
                predicate = predicate.Or(x => x.FirstName.Contains(search.Filter));
                predicate = predicate.Or(x => x.LastName.Contains(search.Filter));
                predicate = predicate.Or(x => x.City.Contains(search.Filter));
                predicate = predicate.Or(x => x.Country.Contains(search.Filter));
                predicate = predicate.Or(x => x.Country.Contains(search.Filter));
            }

            var query = _context.Customers.AsExpandable().Where(predicate);           

            int resultCount = query.Count();

            query = search.SortOrder.ColumnOrder == SortOrder.SortOrderEnum.Asc
                        ? query = query.OrderBy(search.SortOrder.ColumnName) :
                          query = query.OrderByDescending(search.SortOrder.ColumnName) ;

            if (search.Skip.HasValue)
                query = query.Skip(search.Skip.Value);

            if (search.PageSize.HasValue)
                query = query.Take(search.PageSize.Value);

            return new GridResponse<IList<Customer>>()
            {
                Result = query.ToList(),
                ResultCount = resultCount
            };
        }

        public IList<Customer> GetAll()
        {
            return _context.Customers.ToList();
        }

        public Customer GetById(int id)
        {
            return _context.Customers.Where(x => x.Id == id).FirstOrDefault();
        }
    }
}
