﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestNet.SqlDataAccess.Entities;

namespace TestNet.SqlDataAccess
{
    public class TestNetContext : DbContext
    {
        /// <summary>
        /// Gets or sets the Customer dbset.
        /// </summary>
        public DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Initializes a new <see cref="TestNetContext"/> with a DBContextOptions.
        /// </summary>
        public TestNetContext() : base("TestNetContext")
        {

        }

        /// <summary>
        /// On creation of a model, configure the entity.
        /// </summary>
        /// <param name="modelBuilder">The model configuration object.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Customer>()
                .ToTable("customer");
        }
    }
}
