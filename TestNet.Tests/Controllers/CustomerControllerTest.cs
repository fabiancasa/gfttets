﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TestNet;
using TestNet.Controllers;
using TestNet.Domain.Contract.Services;
using TestNet.SqlDataAccess.Entities;

namespace TestNet.Tests.Controllers
{
    [TestClass]
    public class CustomerControllerTest
    {
        private Mock<ICustomerService> _customerService;
        private CustomerController _controller;

        [TestInitialize]
        public void Initialize()
        {
            _customerService = new Mock<ICustomerService>();

            _controller = new CustomerController(_customerService.Object);
        }

        [TestMethod]
        public void Index()
        {
            // Act
            ViewResult result = _controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Edit_Nullable_Id()
        {
            int? id = null;

            _customerService.Setup(s => s.GetById(It.IsAny<int>()))
               .Returns(new Customer());

            // Act
            var result = _controller.Edit(id) as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Edit_WithValue_Id()
        {
            int? id = 1;

            _customerService.Setup(s => s.GetById(It.IsAny<int>()))
               .Returns(new Customer());

            // Act
            var result = _controller.Edit(id) as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Delete()
        {
            int id = 1;

            _customerService.Setup(s => s.GetById(It.IsAny<int>()))
              .Returns(new Customer());

            _customerService.Setup(s => s.Delete(It.IsAny<Customer>()));

            // Act
            var result = _controller.Delete(id) as ViewResult;

            Assert.IsNotNull(result);
        }
    }
}
