﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using TestNet.Domain.Contract.Repositories;
using TestNet.Domain.Contract.Services;
using TestNet.Domain.Services;
using TestNet.Entities;
using TestNet.SqlDataAccess;
using TestNet.SqlDataAccess.Entities;

namespace TestNet.Tests.Domain
{
    [TestClass]
    /// TODO: Repository Test
    public class CustomerServiceTest
    {
        private Mock<ICustomerRepository> _customerRepository;
        private ICustomerService _service;

        [TestInitialize]
        public void Initialize()
        {
            _customerRepository = new Mock<ICustomerRepository>();

            _service = new CustomerService(_customerRepository.Object);
        }

        [TestMethod]
        public void GetAll()
        {
            _customerRepository.Setup(repo => repo.GetAll())
                .Returns(new List<Customer>());

            // Act
            var result = _service.GetAll();

            // Assert
            Assert.IsInstanceOfType(result, typeof(List<Customer>));
        }


        [TestMethod]
        public void GetById()
        {
            Customer cust = new Customer()
            {
                Id = 1,
                Country = "Rosario",
                FirstName ="Fabian",
                LastName = "Casarella"
            };

            _customerRepository.Setup(repo => repo.GetById(It.IsAny<int>()))
                .Returns(cust);

            // Act
            var result = _service.GetById(1);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(Customer));
        }

        [TestMethod]
        public void ValidateInsert()
        {
            Customer cust = new Customer()
            {
                Country = "Rosario",
                FirstName = "Fabian",
                LastName = "Casarella"
            };
            Customer custInserted = new Customer()
            {
                Id = 1,
                Country = "Rosario",
                FirstName = "Fabian",
                LastName = "Casarella"
            };

            _customerRepository.Setup(repo => repo.GetById(It.IsAny<int>()))
                .Returns(custInserted);

            _customerRepository.Setup(repo => repo.Add(cust))
                .Returns(custInserted);

            // Act
            var result = _service.Save(cust);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(Customer));
        }

        [TestMethod]
        public void ValidateUpdate()
        {
            Customer cust = new Customer()
            {
                Id = 1,
                Country = "Rosario",
                FirstName = "Fabian",
                LastName = "Casarella"
            };

            _customerRepository.Setup(repo => repo.GetById(It.IsAny<int>()))
               .Returns(cust);

            _customerRepository.Setup(repo => repo.Update(cust))
                .Returns(cust);

            // Act
            var result = _service.Save(cust);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(Customer));
        }

        [TestMethod]
        public void Find()
        {
            _customerRepository.Setup(repo => repo.Find(It.IsAny<SearchCriteria>()))
                .Returns(new GridResponse<IList<Customer>>()
                {
                    Result = new List<Customer>(),
                    ResultCount = 0
                });

            // Act
            var result = _service.Find(new SearchCriteria());

            // Assert
            Assert.IsInstanceOfType(result, typeof(GridResponse<IList<Customer>>));
        }


        [TestMethod]
        public void Delete()
        {
            Customer cust = new Customer()
            {
                Id = 1,
                Country = "Rosario",
                FirstName = "Fabian",
                LastName = "Casarella"
            };

            _customerRepository.Setup(repo => repo.GetById(It.IsAny<int>()))
               .Returns(cust);

            _customerRepository.Setup(repo => repo.Delete(cust));

            // Act
            _service.Delete(cust);
        }

    }
}
