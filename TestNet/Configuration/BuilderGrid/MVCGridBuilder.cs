﻿using MVCGrid.Models;
using System.Collections.Generic;
using System.Linq;
using TestNet.Domain.Contract.Services;
using TestNet.Entities;
using TestNet.SqlDataAccess.Entities;

namespace TestNet.Configuration.BuilderGrid
{
    public class MVCGridBuilder
    {
        public static MVCGridBuilder<Customer> CustomersGridDefinition()
        {
            return new MVCGridBuilder<Customer>()
                .WithAuthorizationType(AuthorizationType.AllowAnonymous)
                .WithPaging(paging: true, itemsPerPage: 10, allowChangePageSize: true, maxItemsPerPage: 100)
                .WithAdditionalQueryOptionNames("search")
                .AddColumns(cols =>
                {
                    // Add your columns here
                    cols.Add().WithColumnName("Id")
                            .WithHeaderText("Id")
                            .WithValueExpression(i => i.Id.ToString())
                            .WithSorting(false); // use the Value Expression to return the cell text for this column

                    cols.Add().WithColumnName("FirstName")
                            .WithHeaderText("First Name")
                            .WithValueExpression(i => i.FirstName)
                            .WithSorting(true)
                            .WithSortColumnData("FirstName");

                    cols.Add().WithColumnName("LastName")
                        .WithHeaderText("Last Name")
                        .WithValueExpression(i => i.LastName)
                        .WithSorting(true)
                        .WithSortColumnData("LastName");

                    cols.Add().WithColumnName("City")
                        .WithHeaderText("City")
                        .WithValueExpression(i => i.City)
                        .WithSorting(true)
                        .WithSortColumnData("City");

                    cols.Add().WithColumnName("Country")
                        .WithHeaderText("Country")
                        .WithValueExpression(i => i.Country)
                        .WithSorting(true)
                        .WithSortColumnData("Country");

                    cols.Add().WithColumnName("Phone")
                        .WithHeaderText("Phone")
                        .WithValueExpression(i => i.Phone)
                        .WithSorting(true)
                        .WithSortColumnData("Phone");

                    cols.Add().WithColumnName("Edit")
                        .WithHeaderText("")
                        .WithValueTemplate("<a href=\"{Value}\">Edit</a>", false)
                        .WithValueExpression((i, c) => c.UrlHelper.Action("Edit", "Customer", new { id = i.Id }));

                    cols.Add().WithColumnName("Delete")
                        .WithHeaderText("")
                        .WithValueTemplate("<a href=\"{Value}\">Delete</a>", false)
                        .WithValueExpression((i, c) => c.UrlHelper.Action("Delete", "Customer", new { id = i.Id }));
                })                
                .WithSorting(true, "FirstName", SortDirection.Asc)
                .WithPaging(true, 10)
                .WithFiltering(true)
                .WithRetrieveDataMethod((context) =>
                {
                    QueryOptions options = context.QueryOptions;

                    string fa = options.GetFilterString("search");

                    string filter = options.GetAdditionalQueryOptionString("search");

                    ICustomerService service = (ICustomerService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ICustomerService));

                    SearchCriteria search = new SearchCriteria()
                    {
                        SortOrder = new SortOrder()
                        {
                            ColumnName = options.SortColumnName,
                            ColumnOrder = (SortOrder.SortOrderEnum)options.SortDirection
                        },
                        Filter = filter,
                        //OrderBy = options.SortColumnName,                        
                        //SortDirection = options.SortDirection.ToString(),
                        Skip = options.GetLimitOffset().HasValue ? options.GetLimitOffset().Value : 10,
                        PageSize = options.GetLimitOffset().HasValue ?  options.GetLimitRowcount().Value : 10
                    };


                    GridResponse<IList<Customer>> customers = service.Find(search);

                    return new QueryResult<Customer>()
                    {
                        Items = customers.Result,
                        TotalRecords = customers.ResultCount // if paging is enabled, return the total number of records of all pages
                    };
                });
        }
    }
}