[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(TestNet.MVCGridConfig), "RegisterGrids")]

namespace TestNet
{
    using MVCGrid.Web;
    using TestNet.Configuration.BuilderGrid;

    public static class MVCGridConfig 
    {

        public static void RegisterGrids()
        {
            MVCGridDefinitionTable.Add("Customers", MVCGridBuilder.CustomersGridDefinition());
        }
    }
}