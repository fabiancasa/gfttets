﻿using System.Web.Mvc;
using TestNet.Domain.Contract.Services;
using TestNet.SqlDataAccess.Entities;
using TestNet.ViewModels;

namespace TestNet.Controllers
{
    public class CustomerController : BaseController
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            Customer customer = null;

            if (id.HasValue)
            {
                customer = _customerService.GetById(id.Value);
            }
            else
            {
                customer = new Customer();
            }

            CustomerViewModel model = new CustomerViewModel()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                City = customer.City,
                Country = customer.Country,
                Phone = customer.Phone
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            Customer customer = null;

            customer = _customerService.GetById(id);

            CustomerViewModel model = new CustomerViewModel()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                City = customer.City,
                Country = customer.Country,
                Phone = customer.Phone
            };

            return View(model);
        }

        [HttpPost]
        [HandleError()]
        public ActionResult Edit(Customer customer)
        {
            if (ModelState.IsValid)
            {
                _customerService.Save(customer);

                return Redirect("/customer");
            }
            else
            {
                return View(customer);
            }
        }

        [HttpPost]
        public ActionResult Delete(Customer customer)
        {
            _customerService.Delete(customer);

            return Redirect("/customer");
        }
    }
}