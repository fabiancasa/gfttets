﻿using System.Web.Mvc;

namespace TestNet.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return Redirect("/customer");
        }
    }
}