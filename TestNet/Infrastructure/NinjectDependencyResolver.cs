﻿using Ninject;
using Ninject.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TestNet.Domain.Contract.Repositories;
using TestNet.Domain.Contract.Services;
using TestNet.Domain.Services;
using TestNet.SqlDataAccess;
using TestNet.SqlDataAccess.Repositories;

namespace TestNet.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernel)
        {
            this.kernel = kernel;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            kernel.Bind<TestNetContext>().ToSelf().InRequestScope();
            kernel.Bind<ICustomerService>().To<CustomerService>().InRequestScope();
            kernel.Bind<ICustomerRepository>().To<SqlCustomerRepository>().InRequestScope();
        }
    }
}