﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TestNet.Utilities
{
    public interface IViewWriter
    {
        string Write(string viewName, object model);
    }

    public static class ControllerExtensions
    {
        internal static Func<Controller, IViewWriter> WriterFactory = controller => new ViewWriter(controller);

        public static IViewWriter GetViewWriter(this Controller controller)
        {
            return WriterFactory(controller);
        }

        internal class ViewWriter : IViewWriter
        {
            private readonly Controller controller;

            public ViewWriter(Controller controller)
            {
                this.controller = controller;
            }

            public string Write(string viewName, object model)
            {
                using (var writer = new StringWriter())
                {
                    var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                    controller.ViewData.Model = model;
                    var viewCxt = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, writer);
                    viewCxt.View.Render(viewCxt, writer);
                    return writer.ToString();
                }
            }
        }

        public static RouteDto GetReferrer(this Controller controller)
        {
            if (controller.Request == null || controller.Request.UrlReferrer == null)
            {
                return null;
            }

            var fullUrl = controller.Request.UrlReferrer.ToString();
            var index = fullUrl.IndexOf('?');
            string qs = null;
            string url = fullUrl;

            if (index != -1)
            {
                url = fullUrl.Substring(0, index);
                qs = fullUrl.Substring(index + 1);
            }

            var request = new HttpRequest(null, url, qs);
            var response = new HttpResponse(new StringWriter());
            var httpContext = new HttpContext(request, response);

            var routeData = RouteTable.Routes
                .GetRouteData(new HttpContextWrapper(httpContext));

            if (routeData != null)
            {
                var values = routeData.Values;
                var rd = new RouteDto
                {
                    Controller = values["controller"].ToString(),
                    Action = values["action"].ToString()
                };

                if (values.ContainsKey("id"))
                {
                    int id;
                    if (Int32.TryParse(values["id"].ToString(), out id))
                    {
                        rd.Id = id;
                    }
                }

                return rd;
            }

            return null;
        }
    }

    public class RouteDto
    {
        public string Controller { get; set; }

        public string Action { get; set; }

        public int Id { get; set; }
    }
}